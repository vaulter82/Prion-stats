#!/usr/bin/env python
# coding: utf-8



# import necessary libraries

import pandas as pd



# define classes

class Player:
    def __init__(self, name, gender):
        self.name = name
        self.gender = gender



# initialize important variables

stats = pd.read_csv("Prion-stats.csv")
players = []
tournaments = [tourney for tourney in stats.Tournamemnt.unique()]



# define functions

def create_players():
    for player in stats[stats.Passer.notnull()].Passer.unique():
        gender = input("%s (m/f)" % player)
        players.append(Player(player, gender))



# calculate stats

passing = {'total': 0, 'to women': 0, 'to men': 0}

for tourney in tournaments:
    passing[tourney] = {'to women': 0, 'to men': 0}
    for passer in players:
        passing[tourney][passer.name] = {'total': 0, 'to women': 0, 'to men': 0}
        
        for receiver in players:
            if passer == receiver:
                continue

            passes = stats.loc[stats.Passer.isin([passer.name]) & stats.Receiver.isin([receiver.name]) & stats.Tournamemnt.isin([tourney])].shape[0]
            
            passing['total'] += passes
            passing['to men' if receiver.gender == 'm' else 'to women'] += passes
            passing[tourney]['to men' if receiver.gender == 'm' else 'to women'] += passes
            passing[tourney][passer.name]['to men' if receiver.gender == 'm' else 'to women'] += passes
            passing[tourney][passer.name][receiver.name] = passes
            passing[tourney][passer.name]['total'] += passes
